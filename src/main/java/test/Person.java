package test;

import lombok.Data;

@Data
public class Person {

    private String firstName;
    private String lastName;

}
