package test;

public class Main {

    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("world");
        System.out.println("hello, " + person);

        System.out.println(person.hashCode());
        Person person2 = new Person();
        person2.setFirstName("world");
        System.out.println(person.equals(person2));
        System.out.println(person2.hashCode());
        person2.setLastName("Java");
        System.out.println(person.equals(person2));
        System.out.println(person2.hashCode());
    }

}
